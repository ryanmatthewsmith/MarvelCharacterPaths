package hw6.test;

import hw5.DirectedLabeledMultiGraph;

import org.junit.Test;
import hw6.MarvelPaths;

public class TestMarvelPathExceptionThrows {

   @Test (expected=IllegalArgumentException.class)
   public void testNullBuild() {
      MarvelPaths.buildGraph(null);
   }
   
   @Test (expected=IllegalArgumentException.class)
   public void testNullGraphBFS() {
      MarvelPaths.getShortestPath(null,"a","a");
   }
   
   @Test (expected=IllegalArgumentException.class)
   public void testNullOriginBFS() {
      MarvelPaths.getShortestPath(new DirectedLabeledMultiGraph(),null,"a");
   }

   @Test (expected=IllegalArgumentException.class)
   public void testNullDestBFS() {
      MarvelPaths.getShortestPath(new DirectedLabeledMultiGraph(),"a",null);
   }

}
