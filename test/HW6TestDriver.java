package hw6.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;
import java.util.ArrayList;
import hw5.DirectedLabeledMultiGraph;
import hw5.EdgeOut;
import hw6.test.HW6TestDriver;
import hw6.MarvelPaths;


/**
 * This class implements a testing driver which reads test scripts
 * from files for testing Graph, the Marvel parser, and your BFS
 * algorithm.
 **/
public class HW6TestDriver {


   public static void main(String args[]) {
      try {
          if (args.length > 1) {
              printUsage();
              return;
          }

          HW6TestDriver td;

          if (args.length == 0) {
              td = new HW6TestDriver(new InputStreamReader(System.in),
                                     new OutputStreamWriter(System.out));
          } else {

              String fileName = args[0];
              File tests = new File (fileName);

              if (tests.exists() || tests.canRead()) {
                  td = new HW6TestDriver(new FileReader(tests),
                                         new OutputStreamWriter(System.out));
              } else {
                  System.err.println("Cannot read from " + tests.toString());
                  printUsage();
                  return;
              }
          }

          td.runTests();

      } catch (IOException e) {
          System.err.println(e.toString());
          e.printStackTrace(System.err);
      }
  }

  private static void printUsage() {
      System.err.println("Usage:");
      System.err.println("to read from a file: java hw5.test.HW5TestDriver <name of input script>");
      System.err.println("to read from standard in: java hw5.test.HW5TestDriver");
  }

  /** String -> Graph: maps the names of graphs to the actual graph **/
  private final Map<String, DirectedLabeledMultiGraph> graphs = new HashMap<String, DirectedLabeledMultiGraph>();
  private final PrintWriter output;
  private final BufferedReader input;

  /**
   * @requires r != null && w != null
   *
   * @effects Creates a new HW5TestDriver which reads command from
   * <tt>r</tt> and writes results to <tt>w</tt>.
   **/
  public HW6TestDriver(Reader r, Writer w) {
      input = new BufferedReader(r);
      output = new PrintWriter(w);
  }

  /**
   * @effects Executes the commands read from the input and writes results to the output
   * @throws IOException if the input or output sources encounter an IOException
   **/
  public void runTests()
      throws IOException
  {
      String inputLine;
      while ((inputLine = input.readLine()) != null) {
          if ((inputLine.trim().length() == 0) ||
              (inputLine.charAt(0) == '#')) {
              // echo blank and comment lines
              output.println(inputLine);
          }
          else
          {
              // separate the input line on white space
              StringTokenizer st = new StringTokenizer(inputLine);
              if (st.hasMoreTokens()) {
                  String command = st.nextToken();

                  List<String> arguments = new ArrayList<String>();
                  while (st.hasMoreTokens()) {
                      arguments.add(st.nextToken());
                  }

                  executeCommand(command, arguments);
              }
          }
          output.flush();
      }
  }

  private void executeCommand(String command, List<String> arguments) {
      try {
          if (command.equals("CreateGraph")) {
              createGraph(arguments);
          } else if (command.equals("AddNode")) {
              addNode(arguments);
          } else if (command.equals("AddEdge")) {
              addEdge(arguments);
          } else if (command.equals("ListNodes")) {
              listNodes(arguments);
          } else if (command.equals("ListChildren")) {
              listChildren(arguments);
          } else if (command.equals("LoadGraph")) {
              loadGraph(arguments);
          } else if (command.equals("FindPath")) {
              findPath(arguments);
          } else {
              output.println("Unrecognized command: " + command);
          }
      } catch (Exception e) {
          output.println("Exception: " + e.toString());
      }
  }

  private void createGraph(List<String> arguments) {
      if (arguments.size() != 1) {
          throw new CommandException("Bad arguments to CreateGraph: " + arguments);
      }

      String graphName = arguments.get(0);
      createGraph(graphName);
  }

  private void createGraph(String graphName) {
      graphs.put(graphName, new DirectedLabeledMultiGraph());
      output.println("created graph " + graphName);
  }

  private void addNode(List<String> arguments) {
      if (arguments.size() != 2) {
          throw new CommandException("Bad arguments to addNode: " + arguments);
      }

      String graphName = arguments.get(0);
      String nodeName = arguments.get(1);

      addNode(graphName, nodeName);
  }

  private void addNode(String graphName, String nodeName) {
     DirectedLabeledMultiGraph g = graphs.get(graphName);
     g.addVertex(nodeName);
     output.println("added node " + nodeName + " to " + graphName);
  }

  private void addEdge(List<String> arguments) {
      if (arguments.size() != 4) {
          throw new CommandException("Bad arguments to addEdge: " + arguments);
      }

      String graphName = arguments.get(0);
      String parentName = arguments.get(1);
      String childName = arguments.get(2);
      String edgeLabel = arguments.get(3);

      addEdge(graphName, parentName, childName, edgeLabel);
  }

  private void addEdge(String graphName, String parentName, String childName,
          String edgeLabel) {
     
     DirectedLabeledMultiGraph g = graphs.get(graphName);
     g.addEdge(parentName, childName, edgeLabel);

     output.println("added edge " + edgeLabel +  " from " + parentName + " to " + 
                     childName + " in " + graphName);
  }

  private void listNodes(List<String> arguments) {
      if (arguments.size() != 1) {
          throw new CommandException("Bad arguments to listNodes: " + arguments);
      }

      String graphName = arguments.get(0);
      listNodes(graphName);
  }

  private void listNodes(String graphName) {
     DirectedLabeledMultiGraph g = graphs.get(graphName);
     
     StringBuilder s = new StringBuilder();
     Queue<String> v = g.getVerticesAsQueue();
     List<String> vertices = new LinkedList<String>();
     for(String str : v){
        vertices.add(str);
     }
     Collections.sort(vertices);
     
     for(String str : vertices){
        s.append(" " + str);
     }
     
     output.println(graphName + " contains:" + s.toString());
  }

  private void listChildren(List<String> arguments) {
      if (arguments.size() != 2) {
          throw new CommandException("Bad arguments to listChildren: " + arguments);
      }

      String graphName = arguments.get(0);
      String parentName = arguments.get(1);
      listChildren(graphName, parentName);
  }

  private void listChildren(String graphName, String parentName) {
     DirectedLabeledMultiGraph g = graphs.get(graphName);
     StringBuilder s = new StringBuilder();       

     List<EdgeOut> e = g.getChildren(parentName);
     Collections.sort(e);
     for(EdgeOut edge : e){
        s.append(" " + edge.get_vertexTo() + "(" + edge.get_label() + ")");
     }
     output.println("the children of " + parentName + " in " + graphName + " are:" + s);     
  }
  
  private void loadGraph(List<String> arguments) {
     if (arguments.size() != 2) {
         throw new CommandException("Bad arguments to loadName: " + arguments);
     }

     String graphName = arguments.get(0);
     String fileName = arguments.get(1);
     loadGraph(graphName, fileName);
 }

 private void loadGraph(String graphName, String fileName) {    
    
    String filename = "src/hw6/data/" + fileName;
    DirectedLabeledMultiGraph g = MarvelPaths.buildGraph(filename);
    graphs.put(graphName, g);        

    output.println("loaded graph " + graphName);     
 }
 
 private void findPath(List<String> arguments) {
    if (arguments.size() != 3) {
        throw new CommandException("Bad arguments to findPath: " + arguments);
    }
    String graphName = arguments.get(0);
    String node_1 = arguments.get(1).replace('_', ' ');
    String node_n = arguments.get(2).replace('_', ' ');
    findPath(graphName, node_1, node_n);
}

private void findPath(String graphName, String node_1, String node_n) {
   DirectedLabeledMultiGraph g = graphs.get(graphName);
   if (!g.containsVertex(node_1) && !g.containsVertex(node_n)){
      output.println("unknown character " + node_1);
      output.println("unknown character " + node_n);
   } else if (!g.containsVertex(node_1)){
      output.println("unknown character " + node_1);
   } else if(!g.containsVertex(node_n)){
      output.println("unknown character " + node_n);
   } else{
   
      StringBuilder s = new StringBuilder(); 
      s.append("path from " + node_1 + " to " + node_n + ":");
   
      List<EdgeOut> paths = MarvelPaths.getShortestPath(g, node_1, node_n);
      if (paths == null){
         output.println(s.append("\n" + "no path found"));
      } else{
         for (EdgeOut e : paths){
            s.append("\n" + node_1 + " to " + e.get_vertexTo() +
                     " via " + e.get_label());
            node_1 = e.get_vertexTo();
         }
         output.println(s); 
      }
   }
}

  /**
   * This exception results when the input file cannot be parsed properly
   **/
  static class CommandException extends RuntimeException {

      public CommandException() {
          super();
      }
      public CommandException(String s) {
          super(s);
      }

      public static final long serialVersionUID = 3495;
  }
}
