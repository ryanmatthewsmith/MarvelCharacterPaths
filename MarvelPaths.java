package hw6;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Scanner;

import javax.tools.DiagnosticCollector;

import hw5.*;
import hw6.MarvelParser.MalformedDataException;

/**
 *  MarvelPaths is an application that models a 'social network'
 *  of marvel characters with the common comic books the characters
 *  have appeared in.
 *  
 *  MarvelPaths builds a graph from a tsv file in form of 
 *  "character" "book" \n "character" "book" ...
 *  
 *  MarvelPaths also has functionality to find the shortest path
 *  between characters, breaking ties lexicographically,
 *  first by character, then by book title.
 *  
 *  To use as a console application, type names of characters at
 *  the console prompt
 * 
 *  @author RyanMatthewSmith
 *
 */
public class MarvelPaths {
   
   private static String defaultGraphFile = "marvel.tsv";
   
   /**
    *  Builds the graph using data from a .tsv file formatted:
    *  "character" "book" \n "character" "book" \n ...
    *  
    *  @param 'filename' file used to build graph
    *  @requires 'filename' is a valid filepath
    *  @throws IllegalArgumentException if 'filename' is null
    *          OR if the file is not well-formed:
    *          each line contains exactly two tokens separated by a tab,
    *          or else starting with a # symbol to indicate a comment line.
    *  @returns DirectedLabeledMultiGraph representation of 'filename' tsv
    *            
    *           Note: All directed edges are bidirectional, if two characters
    *           appear together in a comic, they both contain an edge with 
    *           the other character and comic title.
    */
   public static DirectedLabeledMultiGraph buildGraph(String filename){
      if (filename == null ){
         throw new IllegalArgumentException("filename can't be null");
      }
      
      DirectedLabeledMultiGraph marvelGraph = new DirectedLabeledMultiGraph();
      
      //run parser with filename and new map
      Set<String> characters = new HashSet<String>(); 
      Map<String, List<String>> books = new HashMap<String,List<String>>();

      try {
         MarvelParser.parseData(filename,characters,books);
      } catch (MalformedDataException e) {
         throw new IllegalArgumentException( filename + " is not well formed");
      }
      
      //add characters into graph first, so edges will be valid
      for(String vertex : characters){
         marvelGraph.addVertex(vertex);  
      }
         
      //add edges
      for (String book : books.keySet()){
         List<String> characterList = books.get(book);
         
         //iterate each character origin 
         for (String origin : characterList){

            //iterate through list adding edges when not same char
            for(String destination : characterList){
               //System.out.print(origin + "," + destination + "," + book);
               if (!(origin.equals(destination))){
                  marvelGraph.addEdge(origin, destination, book);
               }
            }
         }
      }
      return marvelGraph;
   }
   
   /**
    *  Finds shortest path from origin to destination
    *  
    *  @param marvelGraph, graph to be searched for shortest path
    *  @param origin, the intended origin vertex/character
    *  @param destination, the intended destination vertex/character
    *  @throws IllegalArgumentException when: graph, origin, or destination is null
    *                                or when: origin or destination not found in graph
    *  @return shortest path from origin to destination
    *          returns null if no such path exists
    *          Note: for tie-breakers of shortest path, returns lexicographically first
    *                by destination name, then by book name
    */
   public static List<EdgeOut> getShortestPath(DirectedLabeledMultiGraph marvelGraph, 
                                   String origin, String destination){
      if (marvelGraph == null || origin == null || destination == null){
         throw new IllegalArgumentException("getShortestPath doesn't accept null arguments"
                                             + " or empty strings");
      }
      if (!marvelGraph.containsVertex(origin) || !marvelGraph.containsVertex(destination)){
         throw new IllegalArgumentException("origin or destination missing from marvelGraph");
      }
      
      //vertices to be visited
      Queue<String> verticesToVisit = new LinkedList<String>();
      
      //destination vertices, mapped to path from origin
      HashMap<String, List<EdgeOut>> pathsFound = new HashMap<String, List<EdgeOut>>();
      
      //add origin to paths found with empty list
      pathsFound.put(origin, new ArrayList<EdgeOut>());
      
      //add origin to verticesToVisit
      verticesToVisit.add(origin);
      
      //keep checking neighbors or verticesToVisit until list is empty 
      //or path to destination is found
      while (!verticesToVisit.isEmpty()){
         //get edges and sort edges
         String currentOriginVertex = verticesToVisit.poll();
         if (currentOriginVertex.equals(destination)){
            return pathsFound.get(currentOriginVertex);
         }
         
         List<EdgeOut> currentOriginEdges = marvelGraph.getChildren(currentOriginVertex);
         Collections.sort(currentOriginEdges);
         
         //iterate through sorted edges, building paths, return path if destination found
         for(EdgeOut edge : currentOriginEdges){
            String nextVertex = edge.get_vertexTo();
            
            //System.out.println(edge.get_vertexTo() + " " + edge.get_label());
            //key missing, add with path to pathsFound
            //             add nextVertex to worklist, verticesToVisit
            if(!pathsFound.containsKey(nextVertex)){
               List<EdgeOut> originPath = pathsFound.get(currentOriginVertex);
               List<EdgeOut> currentPath = new ArrayList<EdgeOut>(originPath);
               currentPath.add(edge);
               pathsFound.put(nextVertex, currentPath);
               verticesToVisit.add(nextVertex);
            }
         }//end for
      }//end while
      
      //if while terminations, no path exists, return null
      return null;
   }
   
   /**
    * 
    * @param args
    */
   public static void main(String[] args) {
      if (args.length > 1){
         usageNotification();
      }
      if (args.length == 1){
         defaultGraphFile = args[0];
      }
      
      //build graph
      String filename = "src/hw6/data/" + defaultGraphFile;
      DirectedLabeledMultiGraph marvelGraph = MarvelPaths.buildGraph(filename);
      
      //notify user of submission
      System.out.println("MarvelPaths will find the least number of comic books "
                       + "it takes to connect two marvel characters");
      System.out.println("");
      
      Scanner reader = new Scanner(System.in);
      String origin = "";
      String destination = "";
      boolean loopExit = false;
      
      while(!loopExit) {
         System.out.print("Please enter source character name: ");
         origin = reader.nextLine();
         if (!marvelGraph.containsVertex(origin)){
            System.out.println(origin + " not found in graph");
         } else{
            loopExit = true;
         }
      }//while end
      
      loopExit = false;
      while(!loopExit) {
         System.out.print("Please enter destination character name: ");
         destination = reader.nextLine();
         if (!marvelGraph.containsVertex(destination)){
            System.out.println(destination + " not found in graph");
         } else{
            loopExit = true;
         }
      }//while end
      
      StringBuilder pathStringRep = new StringBuilder();
      List<EdgeOut> pathResult = MarvelPaths.getShortestPath(marvelGraph, origin, destination);
      
      if (pathResult == null){
         System.out.println("no path found from " + origin + " to " + destination);
      } else{
         
         //a path exists, build a string line by line, one edge per line
         int connection = 1;
         pathStringRep.append("Shortest path from " + origin + " to " + destination);
         
         for (EdgeOut edge : pathResult){
            pathStringRep.append("\n" + connection + ": " + origin + " appeared with " +
                                 edge.get_vertexTo() + " in " + edge.get_label());
            origin = edge.get_vertexTo();
            connection++;
         }
         System.out.println(pathStringRep);
      }
      reader.close();
   }
   
   //usage notification for main, if > size 1 args 
   private static void usageNotification(){
      System.err.println("MarvelPaths main usage:");
      System.err.println("to use marvel.tsv, no argument to main");
      System.err.println("to use another .tsv, pass \"filename.tsv\" to main");
      System.err.println("file must be place in \"src/hw6/data/file.tsv\"");
   }
}
